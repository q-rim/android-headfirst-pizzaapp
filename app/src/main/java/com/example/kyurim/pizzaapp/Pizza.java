package com.example.kyurim.pizzaapp;

public class Pizza {

    private String name;
    private int imageResourceId;

    private Pizza(String name, int imageResourceId) {
        this.name = name;
        this.imageResourceId = imageResourceId;
    }

    public static final Pizza[] pizzas = {
            new Pizza("diavolo", R.drawable.diavolo),
            new Pizza("funghi", R.drawable.funghi)
    };

    public String getName() {
        return name;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }
}
