package com.example.kyurim.pizzaapp;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CaptionedImagesAdapterHorizontal extends RecyclerView.Adapter<CaptionedImagesAdapterHorizontal.ViewHolder> {

    private String[] captions;
    private int[] imageIds;
    private Listener listener;

    interface Listener {                // inteface for onClick() listener for RecyclerView
        void onClick(int position);
    }

    // Activities and Fragments will use this method to register as a Listener.
    public void setListener(Listener listener) {
        this.listener = listener;       // Register a listener
    }

    public CaptionedImagesAdapterHorizontal(String[] captions, int[] imageIds) {      // Constructor
        this.captions = captions;
        this.imageIds = imageIds;
    }

    @Override
    public CaptionedImagesAdapterHorizontal.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // onCreateViewHolder() method gets called when the RecyclerView needs to create ViewHolder

        // Code to instantiate ViewHolder
        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())         // get LayoutInflator Object
                .inflate(R.layout.card_captioned_image_horizontal, parent, false);      // Use LayoutInflator to turn Layout into CardView

        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // RecyclerView calls this method when it wants to use (or reuse) a ViewHolder for a new piece of data
        CardView cardView = holder.cardView;

        // ImageView - set image
        ImageView imageView = (ImageView) cardView.findViewById(R.id.info_image_horizontal);
        Drawable drawable = ContextCompat.getDrawable(cardView.getContext(), imageIds[position]);
        imageView.setImageDrawable(drawable);
        imageView.setContentDescription(captions[position]);

        // TextView - set text
        TextView textView = (TextView) cardView.findViewById(R.id.info_text_horizontal);
        textView.setText(captions[position]);

        cardView.setOnClickListener(new View.OnClickListener() {        // CardView Listener
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return captions.length;         // number of items in RecyclerView.
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {                    // ViewHolder Inner-Class
        // Define View to be used for each data item.
        // ViewHolder defines what View or Views the RecyclerView should use for each data item given.

        private CardView cardView;
        public ViewHolder(CardView view) {      // Arg: our RecyclerView will display CardViews.
            super(view);                        // call super's constructor
            cardView = view;
        }
    }
}
