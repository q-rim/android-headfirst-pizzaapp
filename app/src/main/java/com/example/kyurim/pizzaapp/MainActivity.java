package com.example.kyurim.pizzaapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    final static String TAG = "---MainActivity";

    private ShareActionProvider shareActionProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);     // get reference to Toolbar Object
        setSupportActionBar(toolbar);                               // we are using Support Library

        // TEMP:  Start OrderActivity
        Intent intent = new Intent(this, OrderActivity.class);

        // Swipe Right
        // Attach FragmentPagerAdapter to the ViewPager
        // we're using support Fragments, so we need to pass our adaptor a reference to the support fragment manager.
        SectionsPagerAdapter pagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        ViewPager pager = (ViewPager) findViewById(R.id.pager);     // attach FragmentPageAdapter to ViewPager
        pager.setAdapter(pagerAdapter);

        // Tab Navigation Menu
        // Attach the ViewPager to the TabLayout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);                        // Link ViewPager to TabLayout
    }

    // AppBar - Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu; Add items to AppBar
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Sharing
        MenuItem menuItem = menu.findItem(R.id.menu_action_share);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);
        setShareActionIntent("Wanna share pizza?");
        return super.onCreateOptionsMenu(menu);
    }

    // sharing
    private void setShareActionIntent(String text) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);     // Create Intent for Sharing to different App
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "test");

        shareActionProvider.setShareIntent(shareIntent);         // share:  setShareIntent()
    }

    // AppBar - React to selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_create_order:
                Log.i(TAG, "Option clicked - case: " + item.getItemId());
                Intent intent = new Intent(this, OrderActivity.class);
                startActivity(intent);
                return true;
            default:
                Log.i(TAG, "Option clicked - default: " + item.getItemId());
                return super.onOptionsItemSelected(item);
        }
    }

    // Fragment Pager Adapter
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {         // specifies which fragment should be displayed on each page
            switch (position) {
                case 0:
                    return new TopFragment();           // we want to display TopFragment first, so we'll return a new instance of it for pos:0 of ViewPager.
                case 1:
                    return new PizzaListFragment();
                case 2:
                    return new PastaListFragment();
                case 3:
                    return new StoresListFragment();
            }
            return null;
        }

        @Override
        public int getCount() {                         // number of pages in the ViewPager
            return 4;                                   // we have total 4 pages(tabs)
        }

        @Override
        public CharSequence getPageTitle(int position) {    // get string for Tab Navigation Menu
            switch (position) {
                case 0:
                    return getResources().getText(R.string.home_tab);
                case 1:
                    return getResources().getText(R.string.pizza_tab);
                case 2:
                    return getResources().getText(R.string.pasta_tab);
                case 3:
                    return getResources().getText(R.string.store_tab);
            }
            return null;
        }
    }
}
