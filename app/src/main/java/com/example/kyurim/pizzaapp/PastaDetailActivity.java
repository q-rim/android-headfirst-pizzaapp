package com.example.kyurim.pizzaapp;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

public class PastaDetailActivity extends AppCompatActivity {

    public static final String EXTRA_PASTA_ID = "pastaId";      // used to passs ID of pasta as Extra info Intent.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasta_detail);

        // Set the Toolbar as Activity's AppBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);      // enable UP Button

        // TextView - Display Pasta Name
        int pastaId = (Integer) getIntent().getExtras().get(EXTRA_PASTA_ID);
        String pastaName = Pasta.pastas[pastaId].getName();
        TextView textView = (TextView) findViewById(R.id.pasta_TextView);
        textView.setText(pastaName);

        // ImageView - Display Pasta Image
        int pastaImage = Pasta.pastas[pastaId].getImageResourceId();
        ImageView ImageView = (ImageView) findViewById(R.id.pasta_ImageView);
        ImageView.setImageDrawable(ContextCompat.getDrawable(this, pastaImage));
        ImageView.setContentDescription(pastaName);
    }
}
